#!/bin/bash

cd /home/neganp/work/grand_analysis/raw

function clean() {
    zip $(date +"%Y.%m.%d").archive.zip gcadj_*
    for year in $(seq 1994 2004); do
        rm -rf gcadj_$year
    done
}

function fetch() {
    for year in $(seq 1994 2004); do
        echo $year
        scp -P23 natep@stetson.phys.dal.ca:/data2/natep/acshual_runs/gcadj_${year}/runs/v8-02-01/geos4/results.zip .
        unzip results.zip; mv results gcadj_${year}
    done
    
    for f in $(find . -name 'avg*.dat'); do
        awk '{if ($2 <= 46) print}' $f > tmpfile
        mv tmpfile $f
    done
}



function dump() {
    for year in $(seq 1994 2004); do
        echo "Handling $year"
        
        ssh natep@stetson.phys.dal.ca -p 23 SGE_ROOT=/grid 'bash -s' <<EOF
SGE_ROOT=/grid
/grid/bin/lx24-amd64/qrsh
cd /data2/natep/acshual_runs/gcadj_${year}/runs/v8-02-01/geos4/
rm -rf results results.zip
cp ~/dump_results.m . && mkdir results
matlab -nodisplay -r dump_results 2&>1  >/dev/null
zip results.zip results/* 2>&1 > /dev/null
EOF
    done
}

case "$1" in
    '')
        echo "What do you want to do?"
        echo "  clean fetch dump"
        ;;
    clean)
        clean
        ;;
    fetch)
        fetch
        ;;
    dump)
        clean
        dump
        ;;
    *)
        echo "I don't know how to do $1"
        echo "valid comands are:"
        echo " * clean"
        echo " * fetch"
        echo " * dump"
esac

