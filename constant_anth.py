#!/usr/bin/python
'''
This script calcaultes biofuel emissions, treating anth as known and 
using assimilated BC and OC totals.
'''

import os
from gc_helpers import years, iterations, plot_ij, LogNorm, na_ocean_mask
import helpers.ij_tools as ij

from emissions import opt_ems_batch, get_totalled_ij
# Import bc and oc by:
#    [bc, oc]
#    [anth, biob, biof, biog, total]
#    years
#    iterations

raw_anth_bc = [get_totalled_ij("bc", yr, 1) for yr in years]
raw_anth_oc = [get_totalled_ij("oc", yr, 1) for yr in years]

def sub(a,b): return a - b

ast_biof_bc = [ij.map_ij(sub,
                         opt_ems_batch["bc", yr, 9],
                         rab) for yr, rab in zip(years, raw_anth_bc)]
ast_biof_oc = [ij.map_ij(sub,
                         opt_ems_batch["oc", yr, 9],
                         rab) for yr, rao in zip(years, raw_anth_oc)]

def fname(spcs, yr):
    name = "{}.{}.png".format(spcs, yr)
    return os.path.join("./plt/const_anth/", name)

if __name__ == "__main__":
    for bc, oc, year in zip(ast_biof_bc, ast_biof_oc, years):
        plot_ij(bc,
                filename=fname("bc", yr))
        plot_ij(oc,
                filename=fname("oc", yr))
