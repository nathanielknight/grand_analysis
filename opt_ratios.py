#!/usr/bin/python
"""
This script plots the ratios log(opt/prior) for different assimilated quantities.
"""
import os
import itertools as it
from helpers.ij_tools import map_ij
import gc_helpers as h
import numpy as np


#Meta
plt_dir = "plt/opt_ratios"
species = ["bc", "oc"]
sectors= ["anth", "biof"]


#Funcs
def search_pattern(spcs, sector, iteration):
    template = "{spcs}_{sector}.{long_spcs}.{iteration}.dat"
    vals = {"spcs": spcs, "sector": sector, "iteration": iteration,
            "long_spcs": {"bc":"blkc", "oc":"orgc"}[spcs]}
    return template.format(**vals)

def log_ratio(opt, prior):
    if prior == 0.0:
        return 0.0
    else:
        return np.log(opt/prior)


def plot(ij, spcs, sector, year):
    title = "Log(opt/prior) for {} {} {}".format(spcs, sector, year)
    filename = os.path.join(plt_dir,
                            "{}_{}_{}.png".format(spcs, sector, year))
    h.plot_ij(ij, filename, title, cblabel="log(opt/prior)",
              vmin=-2,vmax=2)


if __name__ == "__main__":
    for year, dd in zip(h.years, h.data_dirs):
        for spcs, sector in it.product(species, sectors):
            prior = dd.match(search_pattern(spcs, sector, "1")).values()
            opt = dd.match(search_pattern(spcs, sector, str(h.iterations[-1]))).values()
            assert len(prior)==1 and len(opt)==1
            prior, opt = prior[0], opt[0]
            ratio = map_ij(log_ratio, opt, prior)
            plot(ratio, spcs, sector, year)
