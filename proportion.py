#!/usr/bin/python

import gc_helpers as h, re

dd = h.data_dirs[1]

an_ems = h.map_ij(h.add, *dd.match(re.compile("bc_anth.blkc.1.dat")).values())
bf_ems = h.map_ij(h.add, *dd.match(re.compile("bc_biof.blkc.1.dat")).values())
bb_ems = h.map_ij(h.add, *dd.match(re.compile("bc_biob.blkc.1.dat")).values())

def max_frac(a,b,c):
    n = max(a,b,c)
    d = a+b+c
    if d!=0:
        return n/d
    else:
        return -1

f = h.map_ij(max_frac, an_ems, bf_ems, bb_ems)
h.plot_ij(f, "fraction.png", "Fraction emitted by dominant sector", 
          cblabel= "max(sectors)/sum(sectors)",
          vmin=0, vmax=1)
