#!/usr/bin/python
''' Plot scale factors and gradients (the "assimilation info") from the raw datadirs.'''

import os, re, glob
import gc_helpers as gch

#Metadata
years = gch.years
sector_name = {"an": 'Fossil-Fuel', "bb": "Biomass-Burning", "bf":"Bio-fuel"}
iterations = gch.iterations

#Scale Factors
sf_regex = re.compile(r"^ems.(bc|oc)(po|pi)_(an|bf|bb).([1-{}]).dat".format(iterations[-1]))
sf_data = map(lambda dd: dd.match(sf_regex), gch.data_dirs)

def plot_sf(datafile, ij, plots_dir, year=None):

    match = sf_regex.match(datafile)
    species, hygr, sector, iteration = match.groups()

    title = ' '.join([str(year),
                      (species+hygr).upper(), 
                      sector_name[sector], 
                      ", Iteration ", 
                      iteration])
    cblabel = "Scale Factor"
    filename = os.path.join(plots_dir, 
                            "_".join([(species+hygr), sector, str(year), iteration]) + ".png")
    print "Plotting {} for {} in {} ".format(datafile, year, filename)
    gch.plot_ij(ij, filename=filename, title=title, cblabel=cblabel,
                vmin=0, vmax=1)

#Gradients
gdt_regex = re.compile(r"^gde.(bc|oc)(po|pi)_(an|bf|bb).([1-{}]).dat".format(iterations[-1]))
gdt_data = map(lambda dd: dd.match(gdt_regex), gch.data_dirs)

def plot_gdt(datafile, ij, plots_dir, year=None):
    print "Plotting", datafile
    match = gdt_regex.match(datafile)
    species, hygr, sector, iteration = match.groups()
    title = ' '.join([str(year),
                      sector_name[sector],
                      (species+hygr).upper(),
                      "Gradient, Iteration ",
                      iteration])
    cblabel = ''
    filename = os.path.join(plots_dir,
                            "_".join([(species+hygr), sector, str(year), iteration]))
    
    gch.plot_ij(ij, filename=filename, title=title, cblabel=cblabel, norm=gch.SymLogNorm(1e-2),
                vmin=-5e2, vmax=5e2)


if __name__ == "__main__":

    plots_dir = "./plt/cf_gradients"
    for fname in glob.glob(plots_dir + "/*.png"):
        os.remove(fname)
    for year, gdt_datum in zip(years, gdt_data):
        for datafile, ij in gdt_datum.iteritems():
            plot_gdt(datafile, ij, plots_dir, year)

    plots_dir = "./plt/scale_factors"
    for fname in glob.glob(plots_dir + "/*.png"):
        os.remove(fname)
    for year, sf_datum in zip(years, sf_data):
        for datafile, ij in sf_datum.iteritems():
            plot_sf(datafile, ij, plots_dir, year)
