#!/usr/bin/python
"""
This script uses the numpy linear algebra routines to solve invert
bc/oc trends to an/bf trends.
"""

from os.path import join
from emissions import opt_ems_batch
from gc_helpers import years, iterations, plot_ij, LogNorm, na_ocean_mask
import helpers.ij_tools as ij
from helpers.databatch import DataBatch

#Data
an_fraction = ij.read_ij("raw/bcoc_inventory/bcoc_fossil.dat")
bf_fraction = ij.read_ij("raw/bcoc_inventory/bcoc_biofuel.dat")

#Metadata
plt_dir = "plt/sectoral_emissions/"
iteration = iterations[-1]


class SectoralEmissions(DataBatch):
    '''\
    Find the best solution to
    f(x,y) = (ra x + rb y - bc)^2 + ((1-ra) x + (1-rb) y - oc)^2
    obtainable by analytic solution, optimization, or on the boundary of the
    physically realistic region (x & y >= 0).

    Methods are:

    * misfit, the fit functino defined above
    * invert, which returns the best of the solutions among
      * solve
      * optimize
      * boundaries
    * bcoc_emissions, conveniently getting total bc and oc emissions
    * process the DataBatch hook which puts it all together and provides
      map-like access
    '''

    ra = an_fraction
    rb = bf_fraction

    @staticmethod
    def misfit(ra, rb, bc, oc, an, bf):
        return (ra*an + rb* bf - bc)**2 +\
            ((1-ra)*an + (1-rb)*bf - oc)**2

    @staticmethod
    def solve(ra, rb, bc, oc):
        #Analytic solution, may be non-physical
        an_ems = 1.0 / (ra - rb) * (rb * bc + (ra-1.0) * oc)
        bf_ems = 1.0 / (ra - rb) * ((rb-1.0) * bc + ra * oc)
        return an_ems, bf_ems

    @staticmethod
    def optimize(ra, rb, bc, oc):
        #Optimizing solution; may be non-physical
        a = ra**2 + (1-ra)**2
        b = ra * rb + (1-ra)*(1-rb)
        c = b
        d = rb**2 + (1-rb)**2
        if (a*d - b*c) == 0:
            return -1.0, -1.0 #I **HATE** using these as flags, but
            #they'll be caut by self.boundaries
        else:
            an_ems = 1/(a*d - b*c) * (d * bc - b * oc)
            bf_ems = 1/(a*d - b*c) * (-c * bc + a * oc)
            return an_ems, bf_ems

    @staticmethod
    def boundaries(ra, rb, bc, oc):
        #Boundary solutions if the minimum isn't in the free region
        an_ems = (ra * bc + (1.0-ra) * oc) / (ra**2 + (1.0-ra)**2)
        bf_ems = (rb * bc + (1.0-rb) * oc) / (rb**2 + (1.0-rb)**2)
        d_an = (an_ems * ra - bc)**2 + (an_ems * (1.0-ra) - oc)**2
        d_bf = (bf_ems * rb - bc)**2 + (bf_ems * (1.0-ra) - oc)**2
        if d_an < d_bf:
            return an_ems, 0.0
        else:
            return 0.0, bf_ems

    @classmethod
    #Compare analytic, optimizing, and boundary solutions; return best fit
    def invert(cls, ra, rb, bc, oc):
        if (ra == rb):
            return (bc/2 + oc/2), (bc/2 + oc/2)
        else:
            an_ems, bf_ems = cls.solve(ra, rb, bc, oc)
            if (an_ems >= 0) and (bf_ems >=0):
                return an_ems, bf_ems
            else:
                o_an_ems, o_bf_ems = cls.optimize(ra, rb, bc, oc)
                b_an_ems, b_bf_ems = cls.boundaries(ra, rb, bc, oc)
                if cls.misfit(ra, rb, bc, oc, o_an_ems, o_bf_ems) < \
                   cls.misfit(ra, rb, bc, oc, b_an_ems, b_bf_ems):
                    return o_an_ems, o_bf_ems
                else:
                    return b_an_ems, b_bf_ems


    def bcoc_emissions(self, year):
        bc = opt_ems_batch["bc", year, iteration]
        oc = opt_ems_batch["oc", year, iteration]
        return bc, oc

    def __process__(self, year):
        bc, oc = self.bcoc_emissions(year)
        ra, rb = self.ra, self.rb
        an, bf = ij.map_ij(self.invert, ra, rb, bc, oc)
        return an, bf

#data for exporing
sectoral_emissions_batch = SectoralEmissions(years)

class SectoralEmissionPlots(SectoralEmissions):
    def __process__(self, year):
        a, b = sectoral_emissions_batch[year]

        title = "Anth Emissions in {}".format(year)
        filename = join(plt_dir, "{}_{}.png".format("anth", year))
        plot_ij(a, title=title, filename=filename,
                vmin=1.0, vmax=1e8, mask=na_ocean_mask,
                norm=LogNorm())

        title = "Biof Emissions in {}".format(year)
        filename = join(plt_dir, "{}_{}.png".format("biof", year))
        plot_ij(b, title=title, filename=filename,
                vmin=1.0, vmax=1e8, mask=na_ocean_mask,
                norm=LogNorm())


if __name__ == "__main__":
    plot_batch = SectoralEmissionPlots(years)
    #Make all the plots (exhaust the iterator)
    any(plot_batch)
