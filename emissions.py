#!/usr/bin/python
"""
This script plots raw emissions by sector and in total.
"""

import os
import gc_helpers as h
from helpers.ij_tools import map_ij
from helpers.databatch import DataBatch, DataBatchKeyError


#Data Access
#--load
DATA_DIRS = h.data_dirs
YEARS = h.years
data = dict(zip(YEARS, DATA_DIRS))

#--access
_template = "{spcs}_{sector}.{long_spcs}.{iteration}.dat"
def search_pattern(spcs, sector, iteration):
    vals = {"spcs": spcs, "sector": sector, "iteration": iteration,
            "long_spcs": {"bc":"blkc", "oc":"orgc"}[spcs]}
    return _template.format(**vals)



def get_ij(spcs, sector, year, iteration):
    pattern = search_pattern(spcs, sector, iteration)
    ijs = data[year].match(pattern)
    try:
        assert len(ijs) == 1, \
            "Expected 1 result searching {} for {}; got {}".format(
                data[year], pattern, len(ijs))
    except AssertionError:
        raise DataBatchKeyError
    return ijs.values()[0]


def totalled_values(spcs, year, iteration):
    pattern = search_pattern(spcs, ".+", iteration)
    datablocks = data[year].match(pattern).values()
    try:
        total_ems = map_ij(h.add, *datablocks)
    except TypeError as e: #in case of failure, print debugging info
        print pattern
        print datablocks
        raise e
    return total_ems


def get_totalled_ij(spcs, year, iteration):
    try:
        return totalled_values(spcs, year, iteration)
    except DataBatchKeyError:
        pass



class OptimizedEmissionsBatch(DataBatch):
    """
    Conservative set of optimized emissions (just speices, no sector).
    Takes out biomass burning emissions.
    """
    def __process__(self, spcs, year, iteration):
        x = get_totalled_ij(spcs, year, iteration)
        biob = get_ij(spcs, "biob", year, iteration)
        return h.map_ij(lambda a,b: a-b, x, biob)

opt_ems_batch = OptimizedEmissionsBatch(["bc", "oc"],
                                        h.years,
                                        h.iterations)

class AnthBiofEmissionsBatch(DataBatch):
    """
    Conservative set of optimized emissions (just speices, no sector).
    """
    def __process__(self, spcs, year, iteration):
        x = get_totalled_ij(spcs, year, iteration)
        biof = get_ij(spcs, "biof", year, iteration)
        return h.map_ij(lambda a,b: a-b, x, biof)


#Structured results access
class EmissionsBatch(DataBatch):
    '''
    Given lists of species, sectors, years, and iterations, generate plots
    of the results in the ./plt/emissions directory.
    '''
    @staticmethod
    def make_title(spcs, sector, year, iteration):
        return "{} {} Emissions {} - it'n {}".format(spcs.upper(), sector,
                                           year, iteration)

    @staticmethod
    def make_filename(spcs, sector, year, iteration):
        plt_dir = "./plt/emissions/"
        fname = "{}_{}_{}_{}.png".format(spcs, sector, year, iteration)
        return os.path.join(plt_dir, fname)

    @staticmethod
    def get_ij(spcs, sector, year, iteration):
        if sector == "total":
            return get_totalled_ij(spcs, year, iteration)
        else:
            return get_ij(spcs, sector, year, iteration)

    def __process__(self, spcs, sector, year, iteration):
        ij = self.get_ij(spcs, sector, year, iteration)
        title = self.make_title(spcs, sector, year, iteration)
        filename = self.make_filename(spcs, sector, year, iteration)
        h.plot_ij(ij, filename=filename, title=title,
                  cblabel="Emissions", vmin=1, vmax=3e6,
                  norm=h.LogNorm())
        return filename

eb = EmissionsBatch(["bc", "oc"],
                    ["anth", "biob", "biof", "biog", "total"],
                    h.years, h.iterations)



if __name__ == "__main__":
    #Metadata
    for plotfile in eb:
        print "made a plot in {}".format(plotfile)
