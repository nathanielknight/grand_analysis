#!/usr/bin/python
'''
This script plots seasonal averages of GFED data for relevant time periods.
'''
import re

import numpy as np
from gchem import grids as gc_grids
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1 import make_axes_locatable

import data, helpers as h




m_world = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
            llcrnrlon=-180,urcrnrlon=180,resolution='c')
_lon = np.column_stack([gc_grids.e_lon_4x5 for i in range(gc_grids.e_lat_4x5.shape[0])])
_lat = np.row_stack([gc_grids.e_lat_4x5 for i in range(gc_grids.e_lon_4x5.shape[0])])

def plot_world_ij(ij, filename, cblabel='', title='', **kwargs):
    plt.clf()
    ax = plt.subplot(111)
    m_world.drawcountries()
    m_world.drawcoastlines()
    m_world.drawstates()
    x = h.ij_to_array(ij)
    plt.title(title)
    cblabel = ''

    m_world.pcolormesh(_lon, _lat, x, latlon=True, antialiased=True, **kwargs)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    if "label" in kwargs.keys():
        plt.legend()
    cb = plt.colorbar(cax=cax)
    cb.set_label(cblabel)
    plt.savefig(filename, bbox_inches='tight')
    plt.clf()



def get_winter_of(year, spcs, dd):
    months = ["{}01".format(year), "{}02".format(year), "{}12".format(year-1)]
    regexes = ["GFED3.1_{}_{}.txt".format(m, spcs) for m in months]
    ijs = [dd.match(r).values() for r in regexes]
    assert all([len(x)==1 for x in ijs])
    ijs = [ij[0] for ij in ijs]

    total = h.map_ij(h.add, *ijs)
    avg = h.map_ij(lambda x: x / 3.0, total)
    return avg



years = range(1998,2012)
for year in years:
    print "Plotting {}".format(year)
    print "  BC"
    plot_world_ij(
        get_winter_of(year, "BC", data.gfed_dd),
        filename="./plt/bc_{}.png".format(year),
        title="Bioburn BC {}".format(year),
        cblabel="g/m2/month",
        norm=h.LogNorm())
        
    print "  OC"
    plot_world_ij(
        get_winter_of(year, "OC", data.gfed_dd),
        filename="./plt/oc_{}.png".format(year),
        title="Bioburn OC {}".format(year),
        cblabel="g/m2/month",
        norm=h.LogNorm())
    

