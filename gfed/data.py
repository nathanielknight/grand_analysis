#!/usr/bin/python
import os, re
import numpy as np, itertools as it
import gchem.grids


def propper_lon(x):
    return ((x+180.0) % 360.0) - 180.0

def regrid(xs, aggregator, stride):
    def _regrid(xs, ys):
        if len(xs) > 0:
            return _regrid(xs[stride:],
                    ys + [aggregator(xs[:stride])])
        elif xs == []:
            return ys
        else: 
            raise UserWarning, "Seomting went wrong in regrid()"
    return _regrid(list(xs), [])

def regrid_lats(xs):
    xs = np.flipud(xs)
    head = sum(xs[:4])
    last = sum(xs[-4:])
    middle = regrid(xs[4:-4], sum, 8)
    return np.array([head] + middle + [last])
               
                
    


class DataDir():
    _lons = np.arange(-180, 180.5, 0.5)
    _lats = np.arange(-90, 90.5, 0.5)
    _target_lons = gchem.grids.e_lon_4x5
    _target_lats = gchem.grids.e_lat_4x5
    _iis = range(len(_lons)-1)
    _iilen = len(_iis)
    _jjs = range(len(_lats)-1)
    _jjlen = len(_jjs)
        
    def __init__(self, dirname):
        self.dirname = dirname
        self.files = os.listdir(dirname)
    

    def load_array_file(self, filename):
        return np.loadtxt(os.path.join(self.dirname, filename)).transpose()
        #the transpose puts lons in rows (as god intended)

    def load_ij(self, filename):
        a = self.load_array_file(filename)
        return (filename, self.array_to_ij(a))

    def array_to_ij(self, array):
        assert array.shape == (self._iilen, self._jjlen), "{} != {}, {}".format(array.shape, self._iilen, self._jjlen)
        #arange in the same order (and starting pos) as gchem grid
        array = np.roll(array, 6, 0)
        lons_regridded = [regrid(array[:,j], sum, 10) for j in range(self._jjlen)]
        cols = map(lambda a: regrid_lats(np.array(a)), zip(*lons_regridded))
        regridded = np.row_stack(cols)
        return np.row_stack(((i+1,j+1,regridded[i,j]) for i,j in it.product(range(72),range(46))))

            
    def filter(self, f):
        return dict(map(self.load_ij, filter(f, self.files)))

    def match(self, p):
        return self.filter(lambda x: re.match(p, x))
        
        

gfed_dd = DataDir("/home/neganp/work/grand_analysis/gfed/raw")
