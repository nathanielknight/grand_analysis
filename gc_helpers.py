"""
This file contains tools for loading, working with, and plotting data on the geos-chem grid.
"""

import sys, re, os
import itertools as it
import numpy as np
from gchem import grids as gc_grids
from helpers.ij_tools import *


#Results parameters
years = years = range(1994, 2005)
iterations = range(1,8)


#Plotting Concerns
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.colors import Normalize, LogNorm, SymLogNorm
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1 import make_axes_locatable

sym_cmap = plt.cm.RdBu
abs_cmap = plt.cm.Reds

na_ocean_ij = set([tuple(map(float, l.split()))
                   for l in file("./raw/na_ocean_mask.dat").readlines()])
na_ocean_mask = np.array([[bool((i,j) in na_ocean_ij)
                           for j in range(len(gc_grids.c_lat_4x5))]
                          for i in range(len(gc_grids.c_lon_4x5))])


class SymNorm(Normalize):

    def __call__(self, value, clip=None):
        if clip is None:
            cip = self.clip
        result, is_scalar = self.process_value(value)
        self.autoscale_None(result)

        vmin, vmax = self.vmin, self.vmax
        if vmin > 0:
            raise ValueError("minvalue must be less than 0")
        if vmax < 0:
            raise ValueError("maxvalue must be more than 0")
        elif vmin == vmax:
            result.fill(0) # Or should it be all masked? Or 0.5?
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = np.ma.getmask(result)
                result = np.ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                  mask=mask)
            # ma division is very slow; we can take a shortcut
            resdat = result.data

            resdat[resdat>0] /= vmax
            resdat[resdat<0] /= -vmin
            resdat=resdat/2.+0.5
            result = np.ma.array(resdat, mask=result.mask, copy=False)

        if is_scalar:
            result = result[0]

        return result

    def inverse(self, value):
        if not self.scaled():
            raise ValueError("Not invertible until scaled")
        vmin, vmax = self.vmin, self.vmax

        if cbook.iterable(value):
            val = np.ma.asarray(value)
            val=2*(val-0.5) 
            val[val>0]*=vmax
            val[val<0]*=-vmin
            return val
        else:
            if val<0.5: 
                return  2*val*(-vmin)
            else:
                return val*vmax




m = Basemap(projection='eqdc', resolution='l',
            width=6000000, height=3700000,
            lat_1=45., lat_2=55., lat_0=37, lon_0=-98.)
_lon = np.column_stack([gc_grids.e_lon_4x5 for i in range(gc_grids.e_lat_4x5.shape[0])])
_lat = np.row_stack([gc_grids.e_lat_4x5 for i in range(gc_grids.e_lon_4x5.shape[0])])


def plot_ij(ij, filename="tmp.png", title=None, 
            cblabel=None, cbticks=None,
            lon=_lon, lat=_lat,
            mask=na_ocean_mask,
            **kwargs):
    """\
    Use matplotlib and basemap to plot an ij_array to file.

    Arguments:

     * ij: an ij_array to be plotted.
     * filename: the filename to put the resulting plot. The extension
       determines the file-type. (png, jpg, etc.)
     * title, cblabel: title and colorbar-label.
     * cbticks: List-like that determines
     * lon, lat: lattitude and longitude arrays (the same shape as
       ij_to_array(ij)). Can be constructed with np.column_stack and np.row_stack.
    """
    plt.clf()
    ax = plt.subplot(111)
    m.drawcountries()
    m.drawcoastlines()
    m.drawstates()
    x = ij_to_array(ij)
    if mask is not None:
        x = np.ma.masked_array(x, mask)
    if not (title is None):
        plt.title(title)
    if cblabel is None:
        cblabel = ''
    m.pcolormesh(lon, lat, x, latlon=True, antialiased=True, **kwargs)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    if "label" in kwargs.keys():
        plt.legend()
    cb = plt.colorbar(cax=cax)
    cb.set_label(cblabel)
    if cbticks is not None:
        cb.set_ticks(cbticks)
    plt.savefig(filename, bbox_inches='tight')
    plt.clf()

def show_ij(ij, title=None, cblabel=None,
            lon=_lon, lat=_lat, **kwargs):
    plt.clf()
    ax = plt.subplot(111)
    m.drawcountries()
    m.drawcoastlines()
    x = ij_to_array(ij)

    if not (title is None):
        plt.title(title)
    if not (cblabel is None):
        cblabel = ''

    m.pcolormesh(lon, lat, x, latlon=True, antialiased=True, **kwargs)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    if "label" in kwargs.keys():
        plt.legend()
    cb = plt.colorbar(cax=cax)
    cb.set_label(cblabel)
    plt.show()




#GEOS-Chem specific IJ functions
def read_sparse_ij(filename, filler=0.0):
    f = file(filename, 'r')
    ijs = [map(float, a.split()) for a in f.readlines()]
    ijs = {(a,b):c for a,b,c in ijs}
    ij = [[i, j,ijs.get((i,j), filler)] for i,j in 
          it.product(range(1,73), range(1,47))]
    return np.array(ij)

def ij_to_ll(x):
    "Replace an ij_array's indices with geos-chem lon/lat."
    y = x.copy()
    #The -1.0 is necessary to convert from Fortran 1-index arrays to
    #Python 0-indexed arrays.
    y[:,0] = (y[:,0]-1.0)*5.0 - 180
    y[:,1] = (y[:,1]-1.0)*4.0 - 90 + 2.0 #the 2.0 does edges->centers
    return y


def ij_in_na(ij):
    i, j, k = ij
    return (2<i<25) and (26<j<41)

na_ij_mask = np.array(
    [[ (False,False,not ij_in_na((i,j,0))) for j in range(1,47)] for i in range(1,73)])

def restrict_to_na(ij):
    return np.ma.array(ij, mask=na_ij_mask)


def valid_na_data(ij):
    "pull out the k-values of an ijk array that are positive and have indices in the vicinity of north america"
    x = ij_to_ll(ij)
    imask = np.logical_and(np.greater(x[:,0], -150), np.greater(-50, x[:,0]))
    jmask = np.logical_and(np.greater(x[:,1], 20), np.greater(70, x[:,1]))
    kmask = np.greater(x[:,2], 0)
    xmask = np.logical_and(np.logical_and(imask, jmask), kmask)
    return x[:,2][xmask]

iis = range(1, 72+1)
jjs = range(1, 46+1)
lons = gc_grids.e_lon_4x5[:-1]
lats = gc_grids.e_lat_4x5[:-1]
empty_ij = np.array(reduce(lambda x,y: x+y,
                            [[(i,j,0.0) for j in jjs] for i in iis]))


#Handy functions to map onto ij arrays
def add(*args):
    return sum(args)

def divider(default=0.0):
    return lambda a,b: a/b if b!=0 else default

def divide(a,b):
    if b==0:
        return 0.0
    else:
        return a/b

def ppbv_to_conc(ppbv):
    '''
    GEOS-Chem stores carbonaceous aerosol as ppbv mixing ratio (which
    doesn't even make sense!). To convert to ug/m**3,
    we do
    
    mol C          12.017e6 ug/mol C         kg C
    -----     *    --------------     =   ----
    mol air        Vol/mol = (RT/P)       m^3

    with the ideal gas law.
    '''
    return ppbv * 1e-9 * 12e6 / (8.313 * 273.0 / 101.325e3)

def conc_to_ppbv(conc):
    '''
    See ppbv_to_conc for the math.
    '''
    return conc / 1e-9 / 12e6 * (8.313 * 273.0 / 101.325e3)

    

data_dirs = map(IJDir, 
        sorted(map(lambda x: "/home/neganp/work/grand_analysis/raw/gcadj_{}".format(x), years)))
#std_dd = IJDir("./raw/gcadj_std")


#GEOS-Chem 'constants'
gc_area = read_ij("/home/neganp/.utils/geos_chem_4x5_areas.dat")
gc_airvol = read_ij("/home/neganp/.utils/geos_chem_4x5_airvol.dat")
#gc_surface_pressure = read_ij("/home/neganp.utils/geos_chem_4x5_surface_pressure.dat")
