# Grand Analysis of GEOS-Chem Adjoint Output

This directory contains parsing, calculation, and visualization
scripts for my GCADJ results. The main results I want to look at are:

 * **Raw Output**: Concentrations, scale-factors, gradients, and
   raw/assimilated emissions of BC and OC (possibly from an, bf, and
   bb sources).
 * **Proportions**: of anth/biof/biob emissions to total emissions,
   spatially distributed.
 * **Trends**: in raw and assimialted BC/OC and emission types over 1990-2005.
 * **Algebra**: Solving for the trend in emission types that best
   reproduces the assimilated emissions.
 * **Ratios**: of BC/OC for different emission types.

Tools to help do this in an idiomatic way are in `gc_helpers.py` and
`~/.utils/helpers`. PNGs get written in subdirectories of `plt/`, one
directory per result, and can be easily viewed  with my dataviewer
Sinatra app.

-------------------------------------------------------------------------------

## Files
 * [x] **`./raw/*`**: Data dumped from BPCH files by matlab scripts on
   stetson.
   
 * [x] **`./raw/manage.sh`**: Functions for dumping/fetching/cleaning up
   raw data.
   
 * [x] **`gc_helpers.py`**: Tools for loading, manipulating, and visualizing
   data in I,J,K SSV format. 

 * [x] **`asmn_plots.py`**: Cost function gradients and scale-factors
   produced by geos-chem runs.
   
 * [x] **`emissions.py`**: Raw and assimilated emissions by sector and
   aggregated by year.

 * [x] **`opt_ratios.py`**: `log(opt/prior` ratios for sectoral emssions each year

 * [ ] **`algebra.py`**: Do linear algebra to get emissions trends in each cell.

 * [ ] **`proportion.py`**: Plot the ratio of (sectoral emission) / (total
   emission) for black carbon.

--------------------------------------------------------------------------------

 * **`trend.py`**: Calculate the trend of emissions a few different ways.

