#!/usr/bin/python
"""
Examine raw GEOS-Chem emissions to see where Biomas Burning is dominant.
"""

import gc_helpers as h
from results import emission_fractions
import re

bc_regex = re.compile(r"ems.bc.+1.dat")
oc_regex = re.compile(r"ems.oc.+1.dat")

_bb_fraction = 0.06
def mostly_biob_sites(year, datadir):
    #get biomass burning emissions (2nd of 3 components)
    bc_ems = map(lambda x: x[1],
                 sorted(datadir.filter(lambda x: bc_regex.match(x)).iteritems()))
    oc_ems = map(lambda x: x[1],
                 sorted(datadir.filter(lambda x: oc_regex.match(x)).iteritems()))

    tot, an, bb, bf = emission_fractions(*bc_ems)
    bc_bb = h.na_ij(bb)
    
    tot, an, bb, bf = emission_fractions(*oc_ems)
    oc_bb = h.na_ij(bb)


    bc_ijs = set([(x[0], x[1], x[2]) for x in bc_bb if x[-1] > _bb_fraction])
    oc_ijs = set([(x[0], x[1], x[2]) for x in oc_bb if x[-1] > _bb_fraction])

    return bc_ijs.union(oc_ijs)



if __name__ == "__main__":
    banned_indices = reduce(lambda a, b: a.union(b),
                        map(mostly_biob_sites, 
                            ["1990", "1995", "2000", "2005"],
                            h.data_dirs))    
    with file("./tmp/banned_indices", 'w') as outfile:
        for i,j,k in banned_indices:
            outfile.write("{} {}\n".format(i,j))
