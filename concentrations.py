#!/usr/bin/python
'''
This script plots IMPROVE and GEOS-Chem average concentrations 
pre- and post-assimilation for 1995.
'''

import re, os
import itertools as it
import gc_helpers as h
import numpy as np

plt_dir = "./plt/concentrations"



_bc_lim = 3.0
def bc_plots(bc_raw, bc_ast, year):
    bc_raw_conc = h.map_ij(h.ppbv_to_conc, bc_raw)
    bc_ast_conc = h.map_ij(h.ppbv_to_conc, bc_ast)
    bc_log_ratio = h.map_ij(np.log, h.map_ij(h.divider(default=1), bc_ast_conc, bc_raw_conc))
    h.plot_ij(bc_raw_conc, 
              filename=os.path.join(plt_dir, 
                                    "bc_raw_{}".format(year)),
              title="GEOS-Chem Raw BC {}".format(year), cblabel="ug/m^3",
              vmin=0, vmax=0.6)
    h.plot_ij(bc_ast_conc, 
              filename=os.path.join(plt_dir,
                                    "bc_ast_{}.png".format(year)),
              title="GEOS-Chem Assimilated BC {}".format(year), cblabel="ug/m^3",
              vmin=0, vmax=0.6)
    h.plot_ij(bc_log_ratio, 
              filename=os.path.join(plt_dir,
                                    "bc_ratio_{}.png".format(year)),
              title="BC Ln(opt/prior) {}".format(year),
              cblabel='', norm=h.SymNorm(),
              vmin=-_bc_lim, vmax=_bc_lim,
              cmap="RdBu")

_oc_lim = 1.0
def oc_plots(oc_raw, oc_ast, year):
    oc_raw_conc = h.map_ij(h.ppbv_to_conc, oc_raw)
    oc_ast_conc = h.map_ij(h.ppbv_to_conc, oc_ast)
    oc_log_ratio = h.map_ij(np.log, h.map_ij(h.divider(default=1), oc_ast_conc, oc_raw_conc))
    h.plot_ij(oc_raw_conc, 
              filename=os.path.join(plt_dir,
                                    "oc_raw_{}.png".format(year)),
              title="GEOS-Chem Raw OC {}".format(year), cblabel="ug/m^3",
              vmin=0, vmax=3.5)
    h.plot_ij(oc_ast_conc, 
              filename=os.path.join(plt_dir,
                                    "oc_ast_{}.png".format(year)),
              title="GEOS-Chem Assimilated OC {}".format(year), cblabel="ug/m^3",
              vmin=0, vmax=3.5)
    h.plot_ij(oc_log_ratio, 
              filename=os.path.join(plt_dir,
                                    "oc_ratio_{}.png".format(year)),
              title="OC Ln(opt/prior) {}".format(year),
              cblabel='',
              vmin=-_oc_lim, vmax=_oc_lim,
              cmap="RdBu")


def compact(*ijs):
    return h.map_ij(h.add, *ijs)

def average(*ijs):
    s = compact(*ijs)
    l = len(ijs)
    return h.map_ij(lambda x: x/l, s)



if __name__ == "__main__":
    years = h.years

    print "Plotting average BC/OC Concentrations"
    bc_raws = [compact(*dd.filter(lambda x: re.match(r"avg.bc.+1.dat", x)).values())
               for dd in h.data_dirs]
    bc_asts = [compact(*dd.filter(
        lambda x: re.match(r"avg.bc.+{}.dat".format(h.iterations[-1]), x)).values())
              for dd in h.data_dirs]
    bc_raw_avg = average(*bc_raws)
    bc_ast_avg = average(*bc_asts)
    print bc_raw_avg
    bc_plots(bc_raw_avg, bc_ast_avg, "avg")

    oc_raws = [compact(*dd.filter(lambda x: re.match(r"avg.oc.+1.dat", x)).values())
               for dd in h.data_dirs]
    oc_asts = [compact(*dd.filter(lambda x: re.match(r"avg.oc.+{}.dat".format(h.iterations[-1]), x)).values())
              for dd in h.data_dirs]
    oc_raw_avg = average(*oc_raws)
    oc_ast_avg = average(*oc_asts)
    oc_plots(oc_raw_avg, oc_ast_avg, "avg")


    exit() #unless we want annual plots
    for year, dd in zip(years, h.data_dirs):
        print "Plotting bc/oc concentrations for {}".format(year)
        bc_raw = compact(*dd.filter(lambda x: re.match(r"avg.bc.+1.dat", x)).values())
        bc_ast = compact(*dd.filter(
            lambda x: re.match(r"avg.bc.+{}.dat".format(h.iterations[-1]), x)).values())
        bc_plots(bc_raw, bc_ast, year)
        oc_raw = compact(*dd.filter(lambda x: re.match(r"avg.oc.+1.dat", x)).values())
        oc_ast = compact(*dd.filter(lambda x: re.match(r"avg.oc.+{}.dat".format(h.iterations[-1]), x)).values())
        oc_plots(oc_raw, oc_ast, year)
