#!/usr/bin/python
'''\n
This script adds up the BC and OC emissions in each year, calculates trends
over the course of my runs, and plots them.

Its sections are

 * imports and constants
 * amalgamation of data across years
 * functions to generate plots
 * plot generation

Note that since we're interested in trends instead of values, the year doesn't
appear in most of the analysis.
'''
#Imports and Constants
from os.path import join
import numpy as np
from scipy.stats import linregress
import gc_helpers as h
import helpers.ij_tools as ij
#from helpers.theil import theil
from helpers.databatch import DataBatch
import itertools as it

#Data
from emissions import opt_ems_batch
from algebra import sectoral_emissions_batch


#Metadata
plt_dir = "./plt/trends/"
years = h.years
iterations = h.iterations

#Data (for plotting)
def avg(xs):
    return sum(xs) / len(xs)

def var(xs):
    return avg([x**2 for x in xs]) - avg(xs)**2



# Trends =========================================================================
class RelativeEmissionTrendBatch(DataBatch):
    @staticmethod
    def avg(xs):
        if sum(xs) == 0 or len(xs) == 0:
            return 5e5
            #a typical value for carbon aerosol ems in
            #kg/box in north america
        else:
            return sum(xs) / len(xs)

    @staticmethod
    def get_ems(quant):
        emsgetter = {
            "bc": lambda y: opt_ems_batch["bc", y, iterations[-1]],
            "oc": lambda y: opt_ems_batch["oc", y, iterations[-1]],
            "anth": lambda y: sectoral_emissions_batch[y][0],
            "biof": lambda y: sectoral_emissions_batch[y][1]}[quant]

        return map(emsgetter, years)

    def get_avg(self, quant):
        return ij.map_ij(lambda *a: self.avg(a), *self.get_ems(quant))

    @staticmethod
    def trend(*ems):
        '''Filtering trend finder for map_ij.'''
        ems = map(lambda (yr, ems): (yr,ems) if ems>0 else (yr, 1e5), enumerate(ems))
        xs, ys = zip(*ems)
        slope, intercept, rval, pval, stderr = linregress(xs, ys)
        return slope
        
    def __process__(self, quant):
        ems = self.get_ems(quant)
        trend = ij.map_ij(self.trend, *ems)
        avg = self.get_avg(quant)
        return ij.map_ij(lambda t,a: 100.0 * t / a, trend, avg)



class AbsoluteEmissionTrendBatch(RelativeEmissionTrendBatch):
    def __process__(self, quant):
        ems = self.get_ems(quant)
        trend = ij.map_ij(self.trend, *ems)
        return trend
        
relative_emission_trends_batch = RelativeEmissionTrendBatch(["bc", "oc", "anth", "biof"])
absolute_emission_trends_batch = AbsoluteEmissionTrendBatch(["bc", "oc", "anth", "biof"])


# Emissions Diagnostics ==========================================================
class EmissionDiagnosticBatch(RelativeEmissionTrendBatch):
    @staticmethod
    def regression(*ems):
        ems = map(lambda (yr, ems): (yr,ems) if ems>0 else (yr, 1e5), enumerate(ems))
        xs, ys = zip(*ems)
        slope, intercept, rval, pval, stderr = linregress(xs, ys)
        return slope, intercept, rval, pval, stderr

    @classmethod
    def diag_getter(cls, diag):
        ind = dict(zip(
            ["slope", "intercept", "rval", "pval", "stderr"], 
            range(5)))[diag]
        return lambda *ems: cls.regression(*ems)[ind]

    def __process__(self, quant, diag):
        ems = self.get_ems(quant)
        d = ij.map_ij(self.diag_getter(diag), *ems)
        return d

emission_diagnostic_batch = EmissionDiagnosticBatch(["bc", "oc", "anth", "biof"],
                                                      ["pval", "stderr"])




# Plots ==========================================================================
class EmissionTrendPlotBatch(DataBatch):
    '''\n
    Plot species and sector trends, filtering out zero-emissions.
    '''
    trend_sources = {
        "relative": relative_emission_trends_batch,
        "absolute": absolute_emission_trends_batch}
    vlims = {"relative":(-10,10), "absolute":(-1e6, 1e6)}
    cbticks = {
        "relative": None,
        "absolute": [-1e6, -1e4, 0, 1e4, 1e6]
    }

    @staticmethod
    def title(quant, rel):
        if len(quant) ==2 :
            return "{} {} Emission Trend {}-{}".format(
                rel.capitalize(), quant.upper(),
                years[0], years[-1])
        elif len(quant) == 4:
            return "{} {} Emission Trend {}-{}".format(rel.capitalize(), quant.capitalize(),
                                                    years[0], years[-1])
        else:
            raise ValueError("Don't know a title for {}".format(quant))

    @staticmethod
    def filename(quant, rel):
        return join(plt_dir, "{}_{}_trend.png".format(rel, quant))

    @staticmethod
    def get_mask(quant, rel, trend):
        pvalue = emission_diagnostic_batch[quant, "pval"]
        # pmask = ij.map_ij(lambda trend, pvalue: 1 if (pvalue > 0.5) else 0,
        #                     trend, pvalue)
        avg = absolute_emission_trends_batch.get_avg(quant)
        nmask = ij.map_ij(lambda a: 0 if a>=1e5 else 1, avg)
        return np.logical_or(h.na_ocean_mask, ij.ij_to_array(nmask))


    def __process__(self, quant, rel):
        filename = self.filename(quant, rel)
        title = self.title(quant, rel)
        trend = self.trend_sources[rel][quant]
        vmin, vmax = self.vlims[rel]
        mask = self.get_mask(quant, rel, trend)
        cbticks = self.cbticks[rel]
        h.plot_ij(trend,
                  filename=filename,
                  title=title,
                  cblabel="Change/yr [% of avg.]",
                  vmin=vmin, vmax=vmax,
                  mask=mask,
                  cbticks=cbticks,
                  norm=h.SymNorm())


emission_trends_plot_batch = EmissionTrendPlotBatch(["bc", "oc", "anth", "biof"],
                                                             ["relative", "absolute"])



class EmissionDiagnosticPlotBatch(DataBatch):
    vlimits = {
        "pval": (0,1),
        "stderr": (0,1e5)}
    units = {
        "pval": '',
        "stderr": "kg/yr"}

    @staticmethod
    def filename(quant, diag):
        return join(plt_dir, "diagnostic_{}_{}.png".format(quant, diag))

    @staticmethod
    def title(quant, diag):
        return "{} for {} Trend".format(diag.capitalize(), quant.upper())
        

    def __process__(self, quant, diag):
        filename = self.filename(quant, diag)
        title = self.title(quant, diag)
        x = emission_diagnostic_batch[quant, diag]
        vmin, vmax = self.vlimits[diag]
        cblabel = self.units[diag]
        h.plot_ij(x, 
                  filename=filename,
                  title=title,
                  cblabel=cblabel,
                  mask=h.na_ocean_mask,
                  vmin=vmin, vmax=vmax)

emission_diagnostic_plot_batch = EmissionDiagnosticPlotBatch(["bc", "oc", "anth", "biof"],
                                                               ["pval", "stderr"])


if __name__ == "__main__":
    any(emission_trends_plot_batch)
    any(emission_diagnostic_plot_batch)
